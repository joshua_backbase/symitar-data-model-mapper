package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.CurrentAccount
import spock.lang.Specification

class CheckingAccountMapperSpec extends Specification {

    private CheckingAccountMapper checkingAccountMapper = new CheckingAccountMapperImpl()

    void 'mapToCurrentAccount maps a symitar checking share to a backbase current account' () {
        when:
        CurrentAccount result = checkingAccountMapper.mapToCurrentAccount(TestData.checkingShare)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Checking'
            name == 'A FAKE CHECKING SHARE'
            bankAlias == 'A FAKE CHECKING SHARE'
            BBAN == '10000006215858'
            currency == 'USD'
            bookedBalance.amount == 120.00
            bookedBalance.currencyCode == 'USD'
            availableBalance.amount == 100.00
            availableBalance.currencyCode == 'USD'
            debitAccount
            creditAccount

            additions['productCode'] == '1'
            additions['serviceCode'] == '3'
            additions['shareId'] == '0000'
        }
    }
}
