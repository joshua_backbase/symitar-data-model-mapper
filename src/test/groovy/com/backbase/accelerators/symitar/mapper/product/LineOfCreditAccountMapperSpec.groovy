package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.Loan
import spock.lang.Specification

class LineOfCreditAccountMapperSpec extends Specification {

    private LineOfCreditAccountMapper lineOfCreditAccountMapper = new LineOfCreditAccountMapperImpl()

    void 'mapToLineOfCreditAccount maps a symitar line of credit to a backbase loan account' () {
        when:
        Loan result = lineOfCreditAccountMapper.mapToLineOfCreditAccount(TestData.lineOfCredit)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Line of Credit'
            name == 'A FAKE LINE OF CREDIT'
            bankAlias == 'A FAKE LINE OF CREDIT'
            BBAN == '60000006215858'
            currency == 'USD'
            principalAmount.amount == 200.00
            principalAmount.currencyCode == 'USD'
            creditLimit.amount == 300.00
            creditLimit.currencyCode == 'USD'
            outstandingPrincipalAmount == 120.00
            monthlyInstalmentAmount == 20.00
            accountInterestRate == 2.00
            accruedInterest == 10.00
            debitAccount
            creditAccount

            additions['productCode'] == '6'
            additions['serviceCode'] == '3'
            additions['loanCode'] == '2'
            additions['loanId'] == '0060'
            additions['statementBalance'] == '120.00'
            additions['availableCredit'] == '100.00'
        }
    }
}
