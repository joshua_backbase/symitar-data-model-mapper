package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.Loan
import spock.lang.Specification

class LoanAccountMapperSpec extends Specification {

    private LoanAccountMapper loanAccountMapper = new LoanAccountMapperImpl()

    void 'mapToLoanAccount maps a symitar loan to a backbase loan account' () {
        when:
        Loan result = loanAccountMapper.mapToLoanAccount(TestData.loan)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Loan'
            name == 'A FAKE LOAN'
            bankAlias == 'A FAKE LOAN'
            BBAN == '50000006215858'
            currency == 'USD'
            principalAmount.amount == 200.00
            principalAmount.currencyCode == 'USD'
            outstandingPrincipalAmount == 120.00
            monthlyInstalmentAmount == 20.00
            accountInterestRate == 2.00
            accruedInterest == 10.00
            !debitAccount
            creditAccount

            additions['productCode'] == '5'
            additions['serviceCode'] == '1'
            additions['loanCode'] == '1'
            additions['loanId'] == '0050'
            additions['statementBalance'] == '120.00'
        }
    }
}
