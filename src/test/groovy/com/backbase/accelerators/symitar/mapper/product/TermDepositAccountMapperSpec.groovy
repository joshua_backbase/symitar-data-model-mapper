package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.TermDeposit
import spock.lang.Specification

class TermDepositAccountMapperSpec extends Specification {

    private TermDepositAccountMapper termDepositAccountMapper = new TermDepositAccountMapperImpl()

    void 'mapToTermDepositAccount maps a symitar certificate of deposit share to a backbase term deposit account' () {
        when:
        TermDeposit result = termDepositAccountMapper.mapToTermDepositAccount(TestData.certificateOfDepositShare)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Certificate of Deposit'
            name == 'A FAKE CERTIFICATE OF DEPOSIT SHARE'
            bankAlias == 'A FAKE CERTIFICATE OF DEPOSIT SHARE'
            BBAN == '40000006215858'
            accountInterestRate == 5.00
            currency == 'USD'
            bookedBalance.amount == 120.00
            bookedBalance.currencyCode == 'USD'
            principalAmount.amount == 120.00
            principalAmount.currencyCode == 'USD'
            !debitAccount
            !creditAccount

            additions['productCode'] == '4'
            additions['serviceCode'] == '0'
            additions['shareId'] == '0040'
        }
    }
}
