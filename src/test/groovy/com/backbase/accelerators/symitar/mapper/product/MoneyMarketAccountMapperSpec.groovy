package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.CurrentAccount
import spock.lang.Specification

class MoneyMarketAccountMapperSpec extends Specification {

    private MoneyMarketAccountMapper moneyMarketAccountMapper = new MoneyMarketAccountMapperImpl()

    void 'mapToMoneyMarketAccount maps a symitar money market share to a backbase current account' () {
        when:
        CurrentAccount result = moneyMarketAccountMapper.mapToMoneyMarketAccount(TestData.moneyMarketShare)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Money Market'
            name == 'A FAKE MONEY MARKET SHARE'
            bankAlias == 'A FAKE MONEY MARKET SHARE'
            BBAN == '30000006215858'
            currency == 'USD'
            bookedBalance.amount == 120.00
            bookedBalance.currencyCode == 'USD'
            availableBalance.amount == 100.00
            availableBalance.currencyCode == 'USD'
            debitAccount
            creditAccount

            additions['productCode'] == '3'
            additions['serviceCode'] == '3'
            additions['shareId'] == '0030'
            additions['limitedTransactionCount'] == '6'
        }
    }
}
