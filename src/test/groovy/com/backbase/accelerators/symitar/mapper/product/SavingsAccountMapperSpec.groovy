package com.backbase.accelerators.symitar.mapper.product

import com.backbase.accelerators.symitar.TestData
import com.backbase.stream.legalentity.model.SavingsAccount
import spock.lang.Specification

class SavingsAccountMapperSpec extends Specification {

    SavingsAccountMapper savingsAccountMapper = new SavingsAccountMapperImpl()

    void 'mapToSavingsAccount maps a symitar savings share to a backbase savings account' () {
        when:
        SavingsAccount result = savingsAccountMapper.mapToSavingsAccount(TestData.savingsShare)

        then:
        println result

        and:
        verifyAll(result) {
            productTypeExternalId == 'Savings'
            name == 'A FAKE SAVINGS SHARE'
            bankAlias == 'A FAKE SAVINGS SHARE'
            BBAN == '20000006215858'
            currency == 'USD'
            bookedBalance.amount == 120.00
            bookedBalance.currencyCode == 'USD'
            availableBalance.amount == 100.00
            availableBalance.currencyCode == 'USD'
            debitAccount
            creditAccount

            additions['productCode'] == '2'
            additions['serviceCode'] == '3'
            additions['shareId'] == '0010'
            additions['limitedTransactionCount'] == '6'
        }
    }
}
