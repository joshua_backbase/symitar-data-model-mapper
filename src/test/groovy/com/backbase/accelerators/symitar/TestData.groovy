package com.backbase.accelerators.symitar

import com.symitar.generated.symxchange.account.dto.retrieve.Loan
import com.symitar.generated.symxchange.account.dto.retrieve.Share

import javax.xml.datatype.DatatypeFactory
import java.time.LocalDate

class TestData {

    static final Share checkingShare = new Share(
        id: '0000',
        type: 1,
        micrAcctNumber: '10000006215858',
        description: 'A FAKE CHECKING SHARE',
        balance: 120.00,
        availableBalance: 100.00,
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        service: [
            new Share.Service(
                entryId: 1,
                service: 3
            )
        ]
    )

    static final Share savingsShare = new Share(
        id: '0010',
        type: 2,
        micrAcctNumber: '20000006215858',
        description: 'A FAKE SAVINGS SHARE',
        balance: 120.00,
        availableBalance: 100.00,
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        regDCheckCount: 0,
        regDTransferCount: 6,
        service: [
            new Share.Service(
                entryId: 1,
                service: 3
            )
        ]
    )

    static final Share moneyMarketShare = new Share(
        id: '0030',
        type: 3,
        micrAcctNumber: '30000006215858',
        description: 'A FAKE MONEY MARKET SHARE',
        balance: 120.00,
        availableBalance: 100.00,
        regDCheckCount: 0,
        regDTransferCount: 6,
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        service: [
            new Share.Service(
                entryId: 1,
                service: 3
            )
        ]
    )

    static final Share certificateOfDepositShare = new Share(
        id: '0040',
        type: 4,
        micrAcctNumber: '40000006215858',
        description: 'A FAKE CERTIFICATE OF DEPOSIT SHARE',
        balance: 120.00,
        divRate: 5.00,
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        maturityDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2025-01-01').toString()),
        service: [
            new Share.Service(
                entryId: 1,
                service: 0
            )
        ]
    )

    static final Loan loan = new Loan(
        id: '0050',
        type: 5,
        micrAcctNumber: '50000006215858',
        description: 'A FAKE LOAN',
        originalBalance: 200.00,
        balance: 120.00,
        statementNewBalance: 120.00,
        paymentDue: 20.00,
        interestRate: 2.00,
        interestYtd: 10.00,
        loanCode: 1,
        dueDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-02-01').toString()),
        lastPaymentDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        service: [
            new Loan.Service(
                entryId: 1,
                service: 1
            )
        ]
    )

    static final Loan lineOfCredit = new Loan(
        id: '0060',
        type: 6,
        micrAcctNumber: '60000006215858',
        description: 'A FAKE LINE OF CREDIT',
        originalBalance: 200.00,
        balance: 120.00,
        statementNewBalance: 120.00,
        paymentDue: 20.00,
        interestRate: 2.00,
        interestYtd: 10.00,
        creditLimit: 300.00,
        availableCredit: 100.00,
        loanCode: 2,
        dueDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-02-01').toString()),
        lastPaymentDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        openDate: DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDate.parse('2020-01-01').toString()),
        service: [
            new Loan.Service(
                entryId: 1,
                service: 3
            )
        ]
    )
}
