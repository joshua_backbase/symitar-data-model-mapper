package com.backbase.accelerators.symitar.mapper.transaction;

import com.backbase.accelerators.symitar.mapper.BaseMapper;
import com.backbase.dbs.transaction.api.service.v2.model.TransactionsPostRequestBody;
import com.symitar.generated.symxchange.account.dto.retrieve.ShareHold;
import com.symitar.generated.symxchange.account.dto.retrieve.ShareTransaction;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static com.backbase.accelerators.symitar.constants.HoldTransactionType.SHARE_PENDING;
import static com.backbase.accelerators.symitar.constants.MapperConstants.USD;

@Mapper(componentModel = "spring")
public interface MoneyMarketTransactionMapper extends BaseMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "bookingDate", source = "postDate")
    @Mapping(target = "transactionAmountCurrency.currencyCode", constant = USD)
    @Mapping(target = "description", source = "description", defaultValue = "")
    @Mapping(target = "runningBalance", source = "newBalance")
    @Mapping(target = "reference", source = "sequenceNumber")
    @Mapping(target = "checkSerialNumber", source = "draftNumber")
    @Mapping(target = "billingStatus", source = ".", qualifiedByName = "toBillingStatus")
    @Mapping(target = "typeGroup", source = "actionCode", qualifiedByName = "toTransactionTypeGroup")
    @Mapping(target = "type", source = ".", qualifiedByName = "toTransactionType")
    @Mapping(target = "instructedAmountCurrency.amount", source = "newBalance", qualifiedByName = "toAbsoluteValueBigDecimalString")
    @Mapping(target = "instructedAmountCurrency.currencyCode", constant = USD)
    @Mapping(target = "transactionAmountCurrency.amount", source = ".", qualifiedByName = "toShareTransactionAmount")
    @Mapping(target = "creditDebitIndicator", source = "balanceChange", qualifiedByName = "toCreditDebitIndicator")
    TransactionsPostRequestBody mapToTransactionsPostRequestBody(ShareTransaction shareTransaction);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "bookingDate", source = "effectiveDate")
    @Mapping(target = "transactionAmountCurrency.amount", source = "amount", qualifiedByName = "toAbsoluteValueBigDecimalString")
    @Mapping(target = "transactionAmountCurrency.currencyCode", constant = USD)
    @Mapping(target = "description", constant = "")
    @Mapping(target = "billingStatus", source = ".", qualifiedByName = "toBillingStatus")
    @Mapping(target = "typeGroup", constant = SHARE_PENDING)
    @Mapping(target = "type", constant = SHARE_PENDING)
    @Mapping(target = "creditDebitIndicator", source = "amount", qualifiedByName = "toCreditDebitIndicator")
    TransactionsPostRequestBody mapToTransactionsPostRequestBody(ShareHold shareHold);

}
