package com.backbase.accelerators.symitar.mapper.product;

import com.backbase.accelerators.symitar.constants.TransferEligibility;
import com.backbase.accelerators.symitar.mapper.BaseMapper;
import com.backbase.stream.legalentity.model.SavingsAccount;
import com.symitar.generated.symxchange.account.dto.retrieve.Share;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import static com.backbase.accelerators.symitar.constants.MapperConstants.SAVINGS;
import static com.backbase.accelerators.symitar.constants.MapperConstants.USD;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.LIMITED_TRANSACTION_COUNT;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.PRODUCT_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.SERVICE_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.SHARE_ID;

@Mapper(componentModel = "spring")
public interface SavingsAccountMapper extends BaseMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "name", source = "description")
    @Mapping(target = "bankAlias", source = "description")
    @Mapping(target = "productTypeExternalId", constant = SAVINGS)
    @Mapping(target = "currency", constant = USD)
    @Mapping(target = "bookedBalance.amount", source = "balance")
    @Mapping(target = "bookedBalance.currencyCode", constant = USD)
    @Mapping(target = "availableBalance.amount", source = "availableBalance")
    @Mapping(target = "availableBalance.currencyCode", constant = USD)
    @Mapping(target = "BBAN", source = "micrAcctNumber")
    @Mapping(target = "accountOpeningDate", source = "openDate", qualifiedByName = "toOffsetDateTime")
    SavingsAccount mapToSavingsAccount(Share share);

    @AfterMapping
    default void mapAdditionalProperties(@MappingTarget SavingsAccount target, Share source) {
        TransferEligibility transferEligibility = getTransferEligibility(source);

        target.setCreditAccount(transferEligibility.isCredit());
        target.setDebitAccount(transferEligibility.isDebit());
        target.putAdditionsItem(SHARE_ID, source.getId());
        target.putAdditionsItem(LIMITED_TRANSACTION_COUNT, getLimitedTransactionCount(source));
        target.putAdditionsItem(SERVICE_CODE, transferEligibility.getServiceCode().toString());
        target.putAdditionsItem(PRODUCT_CODE, source.getType().toString());
    }
}
