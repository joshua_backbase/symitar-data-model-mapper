package com.backbase.accelerators.symitar.mapper.transaction;

import com.backbase.accelerators.symitar.mapper.BaseMapper;
import com.backbase.dbs.transaction.api.service.v2.model.TransactionsPostRequestBody;
import com.symitar.generated.symxchange.account.dto.retrieve.LoanHold;
import com.symitar.generated.symxchange.account.dto.retrieve.LoanTransaction;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static com.backbase.accelerators.symitar.constants.HoldTransactionType.LOAN_PENDING;
import static com.backbase.accelerators.symitar.constants.MapperConstants.USD;

@Mapper(componentModel = "spring")
public interface LoanTransactionMapper extends BaseMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "bookingDate", source = "postDate", qualifiedByName = "toLocalDate")
    @Mapping(target = "description", source = "description", defaultValue = "")
    @Mapping(target = "runningBalance", source = "newBalance")
    @Mapping(target = "reference", source = "sequenceNumber")
    @Mapping(target = "checkSerialNumber", source = "draftNumber")
    @Mapping(target = "transactionAmountCurrency.amount", source = ".", qualifiedByName = "toLoanTransactionAmount")
    @Mapping(target = "transactionAmountCurrency.currencyCode", constant = USD)
    @Mapping(target = "billingStatus", source = ".", qualifiedByName = "toBillingStatus")
    @Mapping(target = "typeGroup", source = "actionCode", qualifiedByName = "toTransactionTypeGroup")
    @Mapping(target = "type", source = ".", qualifiedByName = "toTransactionType")
    @Mapping(target = "creditDebitIndicator", source = ".", qualifiedByName = "toCreditDebitIndicatorLoanTransaction")
    TransactionsPostRequestBody mapToTransactionsPostRequestBody(LoanTransaction loanTransaction);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "transactionAmountCurrency.amount", source = "amount", qualifiedByName = "toAbsoluteValueBigDecimalString")
    @Mapping(target = "transactionAmountCurrency.currencyCode", constant = USD)
    @Mapping(target = "billingStatus", source = ".", qualifiedByName = "toBillingStatus")
    @Mapping(target = "typeGroup", constant = LOAN_PENDING)
    @Mapping(target = "type", constant = LOAN_PENDING)
    @Mapping(target = "bookingDate", source = "effectiveDate")
    @Mapping(target = "creditDebitIndicator", source = "amount", qualifiedByName = "toCreditDebitIndicatorLoanHold")
    TransactionsPostRequestBody mapToTransactionsPostRequestBody(LoanHold loanHold);
}
