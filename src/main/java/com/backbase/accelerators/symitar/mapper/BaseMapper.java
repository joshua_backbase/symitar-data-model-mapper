package com.backbase.accelerators.symitar.mapper;

import com.backbase.accelerators.symitar.constants.TransactionActionCode;
import com.backbase.accelerators.symitar.constants.TransactionSourceCode;
import com.backbase.accelerators.symitar.constants.TransferEligibility;
import com.backbase.dbs.transaction.api.service.v2.model.CreditDebitIndicator;
import com.symitar.generated.symxchange.account.dto.retrieve.Loan;
import com.symitar.generated.symxchange.account.dto.retrieve.LoanHold;
import com.symitar.generated.symxchange.account.dto.retrieve.LoanTransaction;
import com.symitar.generated.symxchange.account.dto.retrieve.Share;
import com.symitar.generated.symxchange.account.dto.retrieve.ShareHold;
import com.symitar.generated.symxchange.account.dto.retrieve.ShareTransaction;
import org.mapstruct.Named;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.backbase.accelerators.symitar.constants.BillingStatus.BILLED;
import static com.backbase.accelerators.symitar.constants.BillingStatus.EXPIRED;
import static com.backbase.accelerators.symitar.constants.BillingStatus.HOLD;
import static com.backbase.accelerators.symitar.constants.BillingStatus.PENDING;
import static com.backbase.accelerators.symitar.constants.BillingStatus.VOIDED;
import static com.backbase.accelerators.symitar.constants.TransactionType.ACH;
import static com.backbase.accelerators.symitar.constants.TransactionType.ATM;
import static com.backbase.accelerators.symitar.constants.TransactionType.BILL_PAYMENT;
import static com.backbase.accelerators.symitar.constants.TransactionType.CHECK;
import static com.backbase.accelerators.symitar.constants.TransactionType.DEBIT_CARD;
import static com.backbase.accelerators.symitar.constants.TransactionTypeGroup.ADVANCE;
import static com.backbase.accelerators.symitar.constants.TransactionTypeGroup.DEPOSIT;
import static com.backbase.accelerators.symitar.constants.TransactionTypeGroup.OTHER;
import static com.backbase.accelerators.symitar.constants.TransactionTypeGroup.PAYMENT;
import static com.backbase.accelerators.symitar.constants.TransactionTypeGroup.WITHDRAWAL;

public interface BaseMapper {

    @Named("toOffsetDateTime")
    default OffsetDateTime toOffsetDateTime(XMLGregorianCalendar xmlGregorianCalendar) {
        return Optional.ofNullable(xmlGregorianCalendar)
            .map(XMLGregorianCalendar::toGregorianCalendar)
            .map(GregorianCalendar::toZonedDateTime)
            .map(ZonedDateTime::toOffsetDateTime)
            .orElse(null);
    }

    @Named("toLocalDate")
    default LocalDate xMLGregorianCalendarToLocalDate(XMLGregorianCalendar xmlGregorianCalendar) {
        return Optional.ofNullable(xmlGregorianCalendar)
            .map(XMLGregorianCalendar::toGregorianCalendar)
            .map(GregorianCalendar::toZonedDateTime)
            .map(ZonedDateTime::toLocalDate)
            .orElse(null);
    }

    @Named("toBillingStatus")
    default <T> String toBillingStatus(T transaction) {
        if ((transaction instanceof ShareTransaction)) {
            ShareTransaction shareTransaction = (ShareTransaction) transaction;
            convertTransactionVoidCodeToBillingStatus(shareTransaction.getVoidCode());
        } else if (transaction instanceof LoanTransaction) {
            LoanTransaction loanTransaction = (LoanTransaction) transaction;
            return convertTransactionVoidCodeToBillingStatus(loanTransaction.getVoidCode());
        } else if (transaction instanceof ShareHold) {
            ShareHold shareHold = (ShareHold) transaction;
            return convertShareHoldTypeCodeToBillingStatus(shareHold.getType());
        } else if (transaction instanceof LoanHold) {
            LoanHold loanHold = (LoanHold) transaction;
            return convertLoanHoldTypeCodeToBillingStatus(loanHold.getType());
        }

        return null;
    }

    @Named("toTransactionTypeGroup")
    default String toTransactionTypeGroup(String actionCode) {
        if (Objects.isNull(actionCode)) {
            return OTHER.getValue();
        }

        if (actionCode.equalsIgnoreCase(TransactionActionCode.D)) {
            return DEPOSIT.getValue();
        } else if (actionCode.equalsIgnoreCase(TransactionActionCode.W)) {
            return WITHDRAWAL.getValue();
        } else if (actionCode.equalsIgnoreCase(TransactionActionCode.A) || actionCode.equalsIgnoreCase(TransactionActionCode.N)) {
            return ADVANCE.getValue();
        } else if (actionCode.equalsIgnoreCase(TransactionActionCode.P)) {
            return PAYMENT.getValue();
        } else {
            return OTHER.getValue();
        }
    }

    @Named("toTransactionType")
    default <T> String toTransactionType(T transaction) {
        String sourceCode = null;

        if (transaction instanceof ShareTransaction) {
            ShareTransaction shareTransaction = (ShareTransaction) transaction;
            sourceCode = shareTransaction.getSourceCode();
        } else if (transaction instanceof LoanTransaction) {
            LoanTransaction loanTransaction = (LoanTransaction) transaction;
            sourceCode = loanTransaction.getSourceCode();
        }

        if (Objects.isNull(sourceCode)) {
            return OTHER.getValue();
        }

        switch (sourceCode.toUpperCase()) {
            case TransactionSourceCode.D:
                return hasZeroTransferCode(transaction) ? CHECK.getValue() : OTHER.getValue();
            case TransactionSourceCode.E:
                return ACH.getValue();
            case TransactionSourceCode.A:
                return ATM.getValue();
            case TransactionSourceCode.B:
                return BILL_PAYMENT.getValue();
            case TransactionSourceCode.G:
                return DEBIT_CARD.getValue();
            default:
                return OTHER.getValue();
        }
    }

    @Named("toShareTransactionAmount")
    default String toTransactionAmount(ShareTransaction shareTransaction) {
        return shareTransaction.getBalanceChange()
            .add(shareTransaction.getFeeAmount())
            .add(shareTransaction.getInterest())
            .abs()
            .toString();
    }

    @Named("toLoanTransactionAmount")
    default String toLoanTransactionAmount(LoanTransaction loanTransaction) {
        return loanTransaction.getBalanceChange()
            .subtract(loanTransaction.getFeeAmount())
            .subtract(loanTransaction.getInterest())
            .abs()
            .toString();
    }

    @Named("toAbsoluteValueBigDecimalString")
    default String toAbsoluteValueBigDecimalString(BigDecimal bigDecimal) {
        return bigDecimal.abs().toString();
    }

    @Named("toCreditDebitIndicator")
    default CreditDebitIndicator toCreditDebitIndicator(BigDecimal transactionAmount) {
        if (Objects.isNull(transactionAmount)) {
            return null;
        }

        if (isNegativeValue(transactionAmount)) {
            return CreditDebitIndicator.DBIT;
        }

        return CreditDebitIndicator.CRDT;
    }

    @Named("toCreditDebitIndicatorLoanTransaction")
    default CreditDebitIndicator toCreditDebitIndicatorLoanTransaction(LoanTransaction loantransaction) {
        BigDecimal amount = loantransaction.getBalanceChange()
            .subtract(loantransaction.getFeeAmount())
            .subtract(loantransaction.getInterest());

        if (isNegativeValue(amount)) {
            return CreditDebitIndicator.CRDT;
        }

        return CreditDebitIndicator.DBIT;
    }

    @Named("toCreditDebitIndicatorLoanHold")
    default CreditDebitIndicator toCreditDebitIndicatorLoanHold(BigDecimal transactionAmount) {
        if (Objects.isNull(transactionAmount)) {
            return null;
        }

        if (isNegativeValue(transactionAmount)) {
            return CreditDebitIndicator.CRDT;
        }

        return CreditDebitIndicator.DBIT;
    }

    default boolean isNegativeValue(BigDecimal value) {
        return value.compareTo(BigDecimal.ZERO) < 0;
    }

    default <T> boolean hasZeroTransferCode(T transaction) {
        if (transaction instanceof ShareTransaction) {
            return ((ShareTransaction) transaction).getTransferCode() == 0;
        } else if (transaction instanceof LoanTransaction) {
            return ((LoanTransaction) transaction).getTransferCode() == 0;
        } else {
            return false;
        }
    }

    default String convertTransactionVoidCodeToBillingStatus(Short voidCode) {
        return voidCode == 1 ? VOIDED : BILLED;
    }

    default String convertShareHoldTypeCodeToBillingStatus(Short type) {
        if (type == 2 || type == 4 || type == 19) {
            return PENDING;
        } else if (type == 1) {
            return HOLD;
        } else if (type < 0) {
            return EXPIRED;
        }

        return null;
    }

    default String convertLoanHoldTypeCodeToBillingStatus(Short type) {
        if (type == 4 || type == 19) {
            return PENDING;
        } else if (type == 1) {
            return HOLD;
        }

        return null;
    }

    default TransferEligibility getTransferEligibility(Share share) {
        List<Share.Service> serviceCodes = share.getService();

        if (Objects.isNull(serviceCodes)) {
            return TransferEligibility.TRANSFER_NOT_ALLOWED;
        }

        return serviceCodes.stream()
            .filter(serviceCode -> serviceCode.getEntryId() == 1)
            .findFirst()
            .map(Share.Service::getService)
            .map(TransferEligibility::mapServiceCodeToAccountTransferRule)
            .orElse(TransferEligibility.TRANSFER_NOT_ALLOWED);
    }

    default TransferEligibility getTransferEligibility(Loan loan) {
        List<Loan.Service> serviceCodes = loan.getService();

        if (Objects.isNull(serviceCodes)) {
            return TransferEligibility.TRANSFER_NOT_ALLOWED;
        }

        return serviceCodes.stream()
            .filter(serviceCode -> serviceCode.getEntryId() == 1)
            .findFirst()
            .map(Loan.Service::getService)
            .map(TransferEligibility::mapServiceCodeToAccountTransferRule)
            .orElse(TransferEligibility.TRANSFER_NOT_ALLOWED);
    }

    default String getLimitedTransactionCount(Share share) {
        return Optional.of(share.getRegDCheckCount() + share.getRegDTransferCount())
            .map(String::valueOf)
            .orElse(null);
    }

    default OffsetDateTime getNextPaymentDate(Loan loan) {
        // Only map the payment date if we have a positive balance on the loan
        if (Objects.isNull(loan.getBalance()) || loan.getBalance().compareTo(BigDecimal.ZERO) < 0) {
            return null;
        }

        return Optional.ofNullable(loan.getDueDate())
            .map(this::toOffsetDateTime)
            .orElse(null);
    }

    default String getLastPaymentDate(Loan loan) {
        return Optional.ofNullable(loan.getLastPaymentDate())
            .map(this::toOffsetDateTime)
            .map(OffsetDateTime::toString)
            .orElse(null);
    }
}
