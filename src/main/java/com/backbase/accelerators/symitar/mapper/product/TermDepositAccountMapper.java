package com.backbase.accelerators.symitar.mapper.product;

import com.backbase.accelerators.symitar.constants.TransferEligibility;
import com.backbase.accelerators.symitar.mapper.BaseMapper;
import com.backbase.stream.legalentity.model.TermDeposit;
import com.symitar.generated.symxchange.account.dto.retrieve.Share;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import static com.backbase.accelerators.symitar.constants.MapperConstants.CERTIFICATE_OF_DEPOSIT;
import static com.backbase.accelerators.symitar.constants.MapperConstants.USD;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.PRODUCT_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.SERVICE_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.SHARE_ID;

@Mapper(componentModel = "spring")
public interface TermDepositAccountMapper extends BaseMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "name", source = "description")
    @Mapping(target = "bankAlias", source = "description")
    @Mapping(target = "productTypeExternalId", constant = CERTIFICATE_OF_DEPOSIT)
    @Mapping(target = "currency", constant = USD)
    @Mapping(target = "bookedBalance.amount", source = "balance")
    @Mapping(target = "bookedBalance.currencyCode", constant = USD)
    @Mapping(target = "principalAmount.amount", source = "balance")
    @Mapping(target = "principalAmount.currencyCode", constant = USD)
    @Mapping(target = "accountInterestRate", source = "divRate")
    @Mapping(target = "BBAN", source = "micrAcctNumber")
    @Mapping(target = "maturityDate", source = "maturityDate", qualifiedByName = "toOffsetDateTime")
    @Mapping(target = "accountOpeningDate", source = "openDate", qualifiedByName = "toOffsetDateTime")
    TermDeposit mapToTermDepositAccount(Share share);

    @AfterMapping
    default void mapAdditionalProperties(@MappingTarget TermDeposit target, Share source) {
        TransferEligibility transferEligibility = getTransferEligibility(source);

        target.setCreditAccount(transferEligibility.isCredit());
        target.setDebitAccount(transferEligibility.isDebit());
        target.putAdditionsItem(SHARE_ID, source.getId());
        target.putAdditionsItem(SERVICE_CODE, transferEligibility.getServiceCode().toString());
        target.putAdditionsItem(PRODUCT_CODE, source.getType().toString());
    }
}
