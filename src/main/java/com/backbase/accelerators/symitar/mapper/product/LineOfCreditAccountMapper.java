package com.backbase.accelerators.symitar.mapper.product;

import com.backbase.accelerators.symitar.constants.TransferEligibility;
import com.backbase.accelerators.symitar.mapper.BaseMapper;
import com.backbase.stream.legalentity.model.Loan;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import static com.backbase.accelerators.symitar.constants.MapperConstants.LINE_OF_CREDIT;
import static com.backbase.accelerators.symitar.constants.MapperConstants.USD;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.AVAILABLE_CREDIT;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.LAST_PAYMENT_DATE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.LOAN_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.LOAN_ID;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.PRODUCT_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.SERVICE_CODE;
import static com.backbase.accelerators.symitar.constants.ProductAdditions.STATEMENT_BALANCE;

@Mapper(componentModel = "spring")
public interface LineOfCreditAccountMapper extends BaseMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "name", source = "description")
    @Mapping(target = "bankAlias", source = "description")
    @Mapping(target = "productTypeExternalId", constant = LINE_OF_CREDIT)
    @Mapping(target = "currency", constant = USD)
    @Mapping(target = "principalAmount.amount", source = "originalBalance")
    @Mapping(target = "principalAmount.currencyCode", constant = USD)
    @Mapping(target = "outstandingPrincipalAmount", source = "balance")
    @Mapping(target = "creditLimit.amount", source = "creditLimit")
    @Mapping(target = "creditLimit.currencyCode", constant = USD)
    @Mapping(target = "monthlyInstalmentAmount", source = "paymentDue")
    @Mapping(target = "accountInterestRate", source = "interestRate")
    @Mapping(target = "accruedInterest", source = "interestYtd")
    @Mapping(target = "BBAN", source = "micrAcctNumber")
    @Mapping(target = "accountOpeningDate", source = "openDate", qualifiedByName = "toOffsetDateTime")
    Loan mapToLineOfCreditAccount(com.symitar.generated.symxchange.account.dto.retrieve.Loan loan);

    @AfterMapping
    default void mapAdditionalProperties(
        @MappingTarget Loan target,
        com.symitar.generated.symxchange.account.dto.retrieve.Loan source) {

        TransferEligibility transferEligibility = getTransferEligibility(source);

        target.setCreditAccount(transferEligibility.isCredit());
        target.setDebitAccount(transferEligibility.isDebit());
        target.setMinimumPaymentDueDate(getNextPaymentDate(source));
        target.putAdditionsItem(LOAN_ID, source.getId());
        target.putAdditionsItem(SERVICE_CODE, transferEligibility.getServiceCode().toString());
        target.putAdditionsItem(PRODUCT_CODE, source.getType().toString());
        target.putAdditionsItem(LOAN_CODE, String.valueOf(source.getLoanCode()));
        target.putAdditionsItem(LAST_PAYMENT_DATE, getLastPaymentDate(source));
        target.putAdditionsItem(AVAILABLE_CREDIT, String.valueOf(source.getAvailableCredit()));
        target.putAdditionsItem(STATEMENT_BALANCE, String.valueOf(source.getStatementNewBalance()));
    }
}
