package com.backbase.accelerators.symitar.constants;

public class TransactionSourceCode {

    public static final String G = "G"; // Debit Card
    public static final String O = "O";
    public static final String H = "H";
    public static final String K = "K";
    public static final String C = "C";
    public static final String V = "V";
    public static final String D = "D"; // Draft
    public static final String E = "E"; // ACH
    public static final String A = "A"; // ATM
    public static final String B = "B"; // Bill Payment
}
