package com.backbase.accelerators.symitar.constants;

public class ProductAdditions {

    public static final String SHARE_ID = "shareId";
    public static final String LOAN_ID = "loanId";
    public static final String LIMITED_TRANSACTION_COUNT = "limitedTransactionCount";
    public static final String LAST_PAYMENT_DATE = "lastPaymentDate";
    public static final String AVAILABLE_CREDIT = "availableCredit";
    public static final String STATEMENT_BALANCE = "statementBalance";
    public static final String PRODUCT_CODE = "productCode";
    public static final String SERVICE_CODE = "serviceCode";
    public static final String LOAN_CODE = "loanCode";
}
