package com.backbase.accelerators.symitar.constants;

public class MapperConstants {

    public static final String USD = "USD";
    public static final String CHECKING = "Checking";
    public static final String MONEY_MARKET = "Money Market";
    public static final String SAVINGS = "Savings";
    public static final String CERTIFICATE_OF_DEPOSIT = "Certificate of Deposit";
    public static final String LOAN = "Loan";
    public static final String LINE_OF_CREDIT = "Line of Credit";
}
