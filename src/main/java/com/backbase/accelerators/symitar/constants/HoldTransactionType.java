package com.backbase.accelerators.symitar.constants;

public class HoldTransactionType {

    public static final String SHARE_PENDING = "Share Pending";
    public static final String LOAN_PENDING = "Loan Pending";
    public static final String CREDIT_CARD = "Credit Card";
    public static final String ACH_PENDING = "ACH Pending";
}
