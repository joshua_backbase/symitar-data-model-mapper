package com.backbase.accelerators.symitar.constants;

public enum TransferEligibility {

    TRANSFER_IN_AND_OUT(true, true, (short) 3),
    TRANSFER_IN_ONLY(false, true, (short) 1),
    TRANSFER_NOT_ALLOWED(false, false, (short) 0);

    private final boolean isDebit;
    private final boolean isCredit;
    private final Short serviceCode;

    TransferEligibility(boolean isDebit, boolean isCredit, Short serviceCode) {
        this.isDebit = isDebit;
        this.isCredit = isCredit;
        this.serviceCode = serviceCode;
    }

    public boolean isDebit() {
        return isDebit;
    }

    public boolean isCredit() {
        return isCredit;
    }

    public Short getServiceCode() {
        return serviceCode;
    }

    public static TransferEligibility mapServiceCodeToAccountTransferRule(short serviceCode) {
        if (serviceCode == 1) {
            return TRANSFER_IN_ONLY;
        } else if (serviceCode == 3) {
            return TRANSFER_IN_AND_OUT;
        } else {
            return TRANSFER_NOT_ALLOWED;
        }
    }
}

