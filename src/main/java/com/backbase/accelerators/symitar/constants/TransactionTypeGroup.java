package com.backbase.accelerators.symitar.constants;

public enum TransactionTypeGroup {

    WITHDRAWAL("Withdrawal"),
    DEPOSIT("Deposit"),
    ADVANCE("Advance"),
    PAYMENT("Payment"),
    OTHER("Other");

    private final String value;

    TransactionTypeGroup(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
