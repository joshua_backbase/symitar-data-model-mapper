package com.backbase.accelerators.symitar.constants;

public class BillingStatus {

    public static final String BILLED = "BILLED";
    public static final String PENDING = "PENDING";
    public static final String HOLD = "HOLD";
    public static final String VOIDED = "VOIDED";
    public static final String EXPIRED = "EXPIRED";
}

