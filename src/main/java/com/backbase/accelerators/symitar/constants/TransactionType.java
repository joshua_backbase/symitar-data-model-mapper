package com.backbase.accelerators.symitar.constants;

public enum TransactionType {

    CHECK("Check"),
    ACH("ACH"),
    ATM("ATM"),
    BILL_PAYMENT("Bill Payment"),
    DEBIT_CARD("Debit Card");

    private final String value;

    TransactionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
