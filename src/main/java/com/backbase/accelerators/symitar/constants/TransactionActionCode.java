package com.backbase.accelerators.symitar.constants;

public class TransactionActionCode {

    public static final String W = "W"; // Withdrawal
    public static final String D = "D"; // Deposit
    public static final String A = "A"; // Advance/Loan add-on
    public static final String P = "P"; // Loan Payment
    public static final String N = "N"; // New Loan

}
